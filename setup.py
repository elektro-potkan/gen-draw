# ***********************************************
# ***                 GenDraw                 ***
# ***-----------------------------------------***
# *** Elektro-potkan  <dev@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


from setuptools import find_packages, setup


setup(
	name = 'gen-draw',
	author = 'Elektro-potkan',
	author_email = 'dev@elektro-potkan.cz',
	url = 'https://gitlab.com/elektro-potkan/gen-draw',
	use_scm_version = True,
	description = 'Simple drawings generator',
	license = 'GPLv3+',
	keywords = 'draw cad svg modular',
	classifiers = [
		'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
		'Programming Language :: Python :: 3'
	],
	setup_requires = ['setuptools_scm'],
	packages = find_packages(where="src")
)
