# ***********************************************
# ***                 GenDraw                 ***
# ***-----------------------------------------***
# *** Elektro-potkan  <dev@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


from . import coords


class AShape(coords.AObject):
	'''Ancestor of all shapes'''
	
	def __init__(self, coords, properties = {}):
		'''Constructor
		
		@param coords - shape's coordinates
		@param properties - dictionary of additional properties like color, etc. (used directly by compilers, no checks performed before)
		'''
		
		super().__init__(coords)
		
		self._properties = dict(properties)
	# constructor
	
	def getProperties(self):
		'''Returns shape's properties
		
		@return dictionary of properties (independent from the internal one)
		'''
		
		return self._properties.copy()
	# getProperties
	
	def getProperty(self, name):
		'''Returns shape's specific property
		
		@param name - property name to return
		@return property value
		'''
		
		return self._properties[name]
	# getProperty
	
	def setProperty(self, name, value):
		'''Sets shape's specific property
		
		@param name - property name to set
		@param value - property value
		'''
		
		self._properties[name] = value
	# setProperty
# AShape


class Circle(AShape):
	def __init__(self, c, r, properties = {}):
		'''Constructor
		
		@param c - coordinates of center
		@param r - circle radius
		@param properties - shape's properties
		'''
		
		super().__init__(c, properties)
		
		r = float(r)
		if r <= 0:
			raise ValueError('Radius should be a positive number!')
		self._radius = r
	# constructor
	
	def getRadius(self):
		'''Returns circle radius
		
		@return radius
		'''
		
		return self._radius
	# getRadius
	
	def getDiameter(self):
		'''Returns circle diameter
		
		@return diameter
		'''
		
		return self._radius * 2
	# getDiameter
# Circle


class Ellipse(AShape):
	def __init__(self, c, r, properties = {}):
		'''Constructor
		
		@param c - coordinates of center
		@param r - tuple containing ellipse radius in X and Y (according to SVG definition)
		@param properties - shape's properties
		'''
		
		super().__init__(c, properties)
		
		r = (float(r[0]), float(r[1]))
		if r[0] <= 0 or r[1] <= 0:
			raise ValueError('Radius should be a positive number!')
		self._radius = r
	# constructor
	
	def getRadius(self):
		'''Returns ellipse radius
		
		@return (rX, rY)
		'''
		
		return self._radius
	# getRadius
# Ellipse


class Line(AShape):
	def __init__(self, begin, end, properties = {}):
		'''Constructor
		
		@param begin - coordinates of beginning point (and the main coordinates used for relative positioning)
		@param end - either a tuple of X and Y difference from begin (like a vector) or another coordinates instance
			If you specify additional Coords object which is not related to the beginning one, you could simply draw a line attached to another object.
			But be careful, you could simply define a relation, which could cause unwanted effects when relatively positioning whole object,
			which would then move the begin, but not the end of the line.
		@param properties - shape's properties
		'''
		
		super().__init__(begin, properties)
		
		if isinstance(end, coords.Coords):
			self._end = end
		else:
			end = (float(end[0]), float(end[1]))
			if not end[0] and not end[1]:
				raise ValueError('Zero-length line is not permitted!')
			self._end = coords.Coords(self.getCoords(), end)
	# constructor
	
	def getEnd(self):
		'''Returns end point coordinates
		
		@return Coords instance of end point
		'''
		
		return self._end
	# getEnd
	
	def length(self):
		'''Returns absolute length of the line
		
		@return length
		'''
		
		return coords.distance(self.getCoords(), self._end)
	# length
# Line


class Polygon(AShape):
	def __init__(self, pos, points, properties = {}):
		'''Constructor
		
		@param pos - main coordinates used for relative positioning
		@param points - list of coordinates specifying polygon points
			Each point could be specified either by a tuple of X and Y difference from main coordinates (like a vector) or another coordinates instance.
			If you specify additional Coords object which is not related to the main one, you could simply draw a point attached to another object.
			But be careful, you could simply define a relation, which could cause unwanted effects when relatively positioning whole object,
			which would then move part of the polygon, but not the point.
		@param properties - shape's properties
		'''
		
		super().__init__(pos, properties)
		
		if len(points) < 3:
			raise ValueError('Polygon should have at least 3 points!')
		
		self._points = []
		
		for c in points:
			if not isinstance(c, coords.Coords):
				c = (float(c[0]), float(c[1]))
				c = coords.Coords(self.getCoords(), c)
			
			if len(self._points) > 0 and self._points[len(self._points)-1] == c:
				raise ValueError('Zero-length polygon side is not permitted!')
			self._points.append(c)
	# constructor
	
	def getPoints(self):
		'''Returns polygon points
		
		@return list of Coords instances
		'''
		
		return self._points.copy()
	# getPoints
# Polygon


class Rectangle(AShape):
	def __init__(self, pos, width, height, center = False, roundness = None, properties = {}):
		'''Constructor
		
		@param pos - position of rectangle (coordinates of bottom-left corner or center depending on center parameter)
		@param width - rectangle width
		@param height - rectangle height
		@param center - if True, place the rectangle with its center at specified position
		@param roundness
			- None to disable
			- number specifying roundness radius (same in both axis)
			- tuple consisting of roundness radius in X and Y axis (according to SVG definition)
		@param properties - shape's properties
		'''
		
		super().__init__(pos, properties)
		
		# handle dimensions
		width = float(width)
		height = float(height)
		if width <= 0 or height <= 0:
			raise ValueError('Rectangle dimensions should be positive numbers!')
		self._width = width
		self._height = height
		
		# optional centering
		if center:
			self._coords = coords.Coords(self._coords, (-width/2, -height/2))
		
		# handle roundness
		self._roundness = None
		if roundness:
			# optional shortcut for specifying same radius in both axis
			try:
				roundness = (float(roundness[0]), float(roundness[1]))
			except:
				roundness = float(roundness)
				roundness = (roundness, roundness)
			
			if roundness[0] < 0 or roundness[1] < 0:
				raise ValueError('Roundness radius should be a non-negative number!')
			self._roundness = roundness
	# constructor
	
	def getWidth(self):
		'''Returns rectangle width
		
		@return width
		'''
		
		return self._width
	# getWidth
	
	def getHeight(self):
		'''Returns rectangle height
		
		@return height
		'''
		
		return self._height
	# getHeight
	
	def getRoundness(self):
		'''Returns rectangle roundness
		
		@return roundness in (x, y)
		'''
		
		return self._roundness
	# getRoundness
	
	def getDimensions(self):
		'''Returns rectangle dimensions
		
		@return (width, height)
		'''
		
		return (self._width, self._height)
	# getDimensions
# Rectangle


class Text(AShape):
	def __init__(self, pos, text, angle = 0, properties = {}):
		'''Constructor
		
		@param pos - text position (coordinates of first character)
		@param text - string content
		@param angle - rotation of text
		@param properties - shape's properties
		'''
		
		super().__init__(pos, properties)
		
		self._text = str(text)
		self._lines = []
		
		self._angle = float(angle) % 360
	# constructor
	
	def addLine(self, text, spacing = '1em'):
		'''Adds additional line
		
		@param text - line string content
		@param spacing - difference in Y between lines
		@return self
		'''
		
		self._lines.append((str(text), str(spacing)))
		
		return self
	# getLines
	
	def getText(self):
		'''Returns main text
		
		@return text string
		'''
		
		return self._text
	# getText
	
	def getLines(self):
		'''Returns additional lines
		
		@return list of lines
		'''
		
		return self._lines[:]
	# getLines
	
	def getAngle(self):
		'''Returns text rotation
		
		@return angle
		'''
		
		return self._angle
	# getAngle
# Text
