# ***********************************************
# ***                 GenDraw                 ***
# ***-----------------------------------------***
# *** Elektro-potkan  <dev@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import math

from . import transforms


class Coords:
	def __init__(self, master, coords, rad = False):
		'''Constructor
		
		@param master - reference coordinates (set to None for top-level coordinates)
		@param coords
			- tuple of X and Y coordinate for translation
			- angle for rotation
		@param rad - valid only with angle input
			False = angle in degrees
			True = angle in radians
		'''
		
		self._master = master if isinstance(master, Coords) else None
		
		# save transformation
		try:
			len(coords)# just a trigger for try-except
			
			self._angle = 0
			self._coords = (float(coords[0]), float(coords[1]))
			self._matrix = transforms.Txy(self._coords[0], self._coords[1])
		except:
			coords = float(coords)
			if not rad:
				coords = math.radians(coords)
			
			self._angle = coords
			self._coords = (0, 0)
			self._matrix = transforms.R(self._angle, True)
	# constructor
	
	def getMaster(self):
		'''Returns master coordinates
		
		@return Coords instance or None
		'''
		
		return self._master
	# getMaster
	
	def get(self):
		'''Returns relative coordinates (stored ones)
		
		@return (x, y)
		'''
		
		return self._coords
	# get
	
	def getAbsolute(self):
		'''Returns absolute coordinates (= relative to drawing ones)
		
		@return (x, y)
		'''
		
		T = self.getMatrixAbsolute()
		return (T[0][2], T[1][2])
	# getAbsolute
	
	def getAngle(self):
		'''Returns relative rotation angle
		
		@return angle in radians
		'''
		
		return self._angle
	# getAngle
	
	def getAngleAbsolute(self):
		'''Returns absolute rotation angle (= relative to drawing ones)
		
		@return angle (range <0; 2*PI)) in radians
		'''
		
		phi = self._master.getAngleAbsolute() if self._master else 0
		return (phi + self._angle) % (2*math.pi)
	# getAngleAbsolute
	
	def getMatrix(self):
		'''Returns own transformation matrix
		
		@return transforms.Matrix
		'''
		
		return self._matrix
	# getMatrix
	
	def getMatrixAbsolute(self):
		'''Returns absolute transformation matrix
		
		@return transforms.Matrix
		'''
		
		T = self._master.getMatrixAbsolute() if self._master else transforms.Matrix()
		return T * self._matrix
	# getMatrixAbsolute
	
	def getMatrixChain(self):
		'''Returns chain of all transformation matrices
		
		@return [transforms.Matrix]
		'''
		
		chain = self._master.getMatrixChain() if self._master else []
		chain.append(self._matrix)
		return chain
	# getMatrixChain
# Coords


class AObject:
	'''Ancestor of positionable objects'''
	
	def __init__(self, coords):
		'''Constructor
		
		@param coords - coordinates
		'''
		
		if not isinstance(coords, Coords):
			raise ValueError('Every object needs to know its coordinates!')
		self._coords = coords
	# constructor
	
	def getCoords(self):
		'''Returns object's coordinates
		
		@return Coords instance
		'''
		
		return self._coords
	# getCoords
# AObject


def difference(c1, c2):
	'''Returns the difference (distances in X and Y) of given coordinates
	
	@param c1 - first Coords instance
	@param c2 - second Coords instance
	@return differences (dX, dY)
	'''
	
	if not isinstance(c1, Coords) or not isinstance(c2, Coords):
		raise ValueError('The arguments needs to be coordinates!')
	
	c1 = c1.getAbsolute()
	c2 = c2.getAbsolute()
	
	return ((c1[0] - c2[0]), (c1[1] - c2[1]))
# difference

def distance(c1, c2):
	'''Returns the absolute distance of given coordinates
	
	@param c1 - first Coords instance
	@param c2 - second Coords instance
	@return distance (Euler's)
	'''
	
	if not isinstance(c1, Coords) or not isinstance(c2, Coords):
		raise ValueError('The arguments needs to be coordinates!')
	
	c1 = c1.getAbsolute()
	c2 = c2.getAbsolute()
	
	return math.sqrt((c1[0] - c2[0])**2 + (c1[1] - c2[1])**2)
# distance
