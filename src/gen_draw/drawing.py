# ***********************************************
# ***                 GenDraw                 ***
# ***-----------------------------------------***
# *** Elektro-potkan  <dev@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


from .coords import AObject, Coords


class Drawing(AObject):
	def __init__(self, coords = None):
		'''Constructor
		
		@param coords - None or coordinates if used as modular sub-drawing
		'''
		
		if not coords:
			coords = Coords(None, (0, 0))
		super().__init__(coords)
		
		self._objectsBefore = []
		self._objects = []
		self._objectsAfter = []
	# constructor
	
	def _add(self, obj, group):
		'''Adds an object to drawing into ther right group
		
		@param obj - AObject instance
		@param group - internal group for the object
		'''
		
		if not isinstance(obj, AObject):
			raise ValueError('Given object is not a successor of positioned object!')
		
		group.append(obj)
	# _add
	
	def addBefore(self, obj):
		'''Adds an object to drawing (including sub-drawing) to be rendered under the others
		
		@param obj - AObject instance
		@return self
		'''
		
		self._add(obj, self._objectsBefore)
		
		return self
	# addBefore
	
	def add(self, obj):
		'''Adds an object to drawing (including sub-drawing)
		
		@param obj - AObject instance
		@return self
		'''
		
		self._add(obj, self._objects)
		
		return self
	# add
	
	def addAfter(self, obj):
		'''Adds an object to drawing (including sub-drawing) to be rendered on top of the others
		
		@param obj - AObject instance
		@return self
		'''
		
		self._add(obj, self._objectsAfter)
		
		return self
	# addAfter
	
	def objects(self):
		'''Returns drawing objects
		
		@return list of objects (independent from the internal ones)
		'''
		
		return self._objectsBefore + self._objects + self._objectsAfter
	# objects
# Drawing
