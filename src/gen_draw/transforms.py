# ***********************************************
# ***                 GenDraw                 ***
# ***-----------------------------------------***
# *** Elektro-potkan  <dev@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


from math import cos, radians, sin


class Matrix:
	'''Transformation 2D matrix in homogenous coordinates'''
	
	def __init__(self, coef = ((1, 0, 0), (0, 1, 0))):
		'''Constructor
		
		@param coef - 2x3 array of matrix coefficients (last row to 3x3 will be completed automatically)
		'''
		
		self._data = (
			(coef[0][0], coef[0][1], coef[0][2]),
			(coef[1][0], coef[1][1], coef[1][2]),
			(0, 0, 1)
		)
	# constructor
	
	def __getitem__(self, key):
		'''Returns matrix data'''
		
		return self._data[key] # safe as these are always immutable tuples
	# __getitem__
	
	def __mul__(self, multiplier):
		'''Matrix multiplication'''
		
		if not isinstance(multiplier, Matrix):
			raise ValueError("The multiplier must be of the same Matrix type! No support for any other type.")
		
		ret = [[0, 0, 0], [0, 0, 0]]
		
		#TODO very slow and non-optimal (but it just works for now)
		for i in range(2):
			for j in range(3):
				for k in range(3):
					ret[i][j] += self._data[i][k] * multiplier._data[k][j]
		
		return Matrix(ret)
	# __mul__
# Matrix


def Tx(d):
	'''Returns X-translation matrix
	
	@param d - distance in X
	'''
	
	return Matrix((
		(1, 0, d),
		(0, 1, 0)
	))
# Tx

def Ty(d):
	'''Returns Y-translation matrix
	
	@param d - distance in Y
	'''
	
	return Matrix((
		(1, 0, 0),
		(0, 1, d)
	))
# Ty

def Txy(dx, dy):
	'''Returns XY-translation matrix
	
	@param dx - distance in X
	@param dy - distance in Y
	'''
	
	return Matrix((
		(1, 0, dx),
		(0, 1, dy)
	))
# Txy

def R(phi, rad = False):
	'''Returns rotation matrix
	
	@param phi - angle
	@param rad
		False = angle in degrees
		True = angle in radians
	'''
	
	if not rad:
		phi = radians(phi)
	
	return Matrix((
		(cos(phi), -sin(phi), 0),
		(sin(phi),  cos(phi), 0)
	))
# R
