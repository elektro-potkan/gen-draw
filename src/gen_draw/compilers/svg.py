# ***********************************************
# ***                 GenDraw                 ***
# ***-----------------------------------------***
# *** Elektro-potkan  <dev@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


from xml.sax.saxutils import escape, quoteattr

from ..drawing import Drawing
from .. import shapes


UNITS = 'mm'


def compile(drawing, polygonLines = False):
	'''Compile drawing into SVG
	
	@param drawing - Drawing instance
	@param polygonLines - compile polygons into sequence of lines (otherwise one could not use real-world units polygons)
	@return SVG text
	'''
	
	if not isinstance(drawing, Drawing):
		raise ValueError('Compiler needs an instance Drawing!')
	
	out = header()
	out += process(drawing, 1, polygonLines)
	out += footer()
	
	return out
# compile

def header():
	'''Returns SVG header'''
	
	return '<?xml version="1.0" encoding="UTF-8" ?>\n<svg>\n'
# header

def footer():
	'''Returns SVG footer'''
	
	return '</svg>\n'
# footer

def process(drawing, level = 0, polygonLines = False):
	'''Process drawing and its objects recursively
	
	@param drawing - Drawing instance
	@param level - the indentation level
	@return SVG text
	
	Every drawing will be placed in its own SVG group.
	'''
	
	out = level*'\t' + '<g>\n'
	
	for obj in drawing.objects():
		out += process(obj, level+1, polygonLines) if isinstance(obj, Drawing) else processShape(obj, level+1, polygonLines)
	
	out += level*'\t' + '</g>\n'
	
	return out
# process

def processShape(obj, level = 0, polygonLines = False):
	'''Process shape
	
	@param obj - AShape instance
	@param level - the indentation level
	@return SVG text
	'''
	
	if not isinstance(obj, shapes.AShape):
		raise ValueError('Needs a shape!')
	
	# shape-specific tasks
	if isinstance(obj, shapes.Circle):
		tag = ('circle',
			outputObjectCoords(obj, pre='c')
			+ ' r="' + str(obj.getRadius()) + UNITS+'"'
		)
	elif isinstance(obj, shapes.Ellipse):
		tag = ('ellipse',
			outputObjectCoords(obj, pre='c')
			+ outputXY(obj.getRadius(), pre='r')
		)
	elif isinstance(obj, shapes.Line):
		tag = ('line',
			outputObjectCoords(obj, post='1')
			+ outputCoords(obj.getEnd(), post='2')
		)
	elif isinstance(obj, shapes.Polygon):
		# convert polygon to group of lines
		if polygonLines:
			pd = Drawing(obj.getCoords())
			
			pts = obj.getPoints()
			c2 = pts[len(pts)-1]
			for c in pts:
				c1 = c2
				c2 = c
				pd.add(shapes.Line(c1, c2, properties=obj.getProperties()))
			
			return process(pd, level)
		
		tag = ('polygon',
			' points="' + ' '.join([(str(c.getAbsolute()[0]) + ',' + str(c.getAbsolute()[1])) for c in obj.getPoints()]) + '"'
		)
	elif isinstance(obj, shapes.Rectangle):
		# fix flipped coordinates
		xy = obj.getCoords().getAbsolute()
		xy = list(xy) # cast to allow item assignment
		xy[1] += obj.getHeight()
		
		tag = ('rect',
			outputXY((xy[0], -xy[1])) # flip it like in outputCoords method
			+ ' width="' + str(obj.getWidth()) + UNITS+'"'
			+ ' height="' + str(obj.getHeight()) + UNITS+'"'
			+ (outputXY(obj.getRoundness(), pre='r') if obj.getRoundness() else '')
		)
	elif isinstance(obj, shapes.Text):
		if len(obj.getLines()) > 0:
			tmp = '\n'
			
			if obj.getText() != '':
				tmp += (level+1)*'\t' + escape(obj.getText()) + '\n'
			
			# handle additional lines
			for l in obj.getLines():
				tmp += (level+1)*'\t' + '<tspan x="'+str(obj.getCoords().getAbsolute()[0])+UNITS+'" dy="'+l[1]+'">' + escape(l[0]) + '</tspan>' + '\n'
			
			tmp += level*'\t'
		else:
			tmp = escape(obj.getText())
		
		tag = ('text',
			outputObjectCoords(obj)
			+ (' transform="rotate(' + str(obj.getAngle()) + ', ' + str(obj.getCoords().getAbsolute()[0]) + ', ' + str(obj.getCoords().getAbsolute()[1]) + ')"' if obj.getAngle() else '')
			,
			tmp
		)
	else:
		raise ValueError('Object unknown to SVG compiler!')
	
	# XML beginning
	out = level*'\t' + '<' + tag[0]
	
	if len(tag) > 1:
		out += tag[1]
	
	# XML properties
	props = obj.getProperties()
	for p in props:
		out += ' ' + p + '=' + quoteattr(str(props[p]))
	
	# XML end
	out += ' />' if len(tag) < 3 or tag[2] == None else '>' + tag[2] + '</'+tag[0]+'>'
	
	return out + '\n'
# processShape

def outputObjectCoords(obj, pre = '', post = ''):
	'''Returns absolute coordinates text of given object
	
	@param obj - AObject instance
	@param pre - SVG attribute prefix
	@param post - SVG attribute postfix
	@return SVG attributes text
	'''
	
	return outputCoords(obj.getCoords(), pre, post)
# outputObjectCoords

def outputCoords(c, pre = '', post = ''):
	'''Returns absolute coordinates text
	
	@param c - Coords instance
	@param pre - SVG attribute prefix
	@param post - SVG attribute postfix
	@return SVG attributes text
	'''
	
	xy = c.getAbsolute()
	xy = (xy[0], -xy[1])
	
	return outputXY(xy, pre, post)
# outputCoords

def outputXY(xy, pre = '', post = ''):
	'''Returns SVG attributes from given (x, y) tuple
	
	@param xy - (x, y) tuple
	@param pre - SVG attribute prefix
	@param post - SVG attribute postfix
	@return SVG attributes text
	'''
	
	return ' ' + pre+'x'+post + '="'+str(xy[0])+UNITS+'" ' + pre+'y'+post + '="'+str(xy[1])+UNITS+'"'
# outputXY
