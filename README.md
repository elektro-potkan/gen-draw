GenDraw = Simple drawings generator
===================================
Allows preparing of simple drawings using modular approach.

Created to allow generating of basic SVG drawing of panel in a modular way
with the ability to add the final absolute position of holes for drilling (imported from modules) into it.

The goal is to use the same concept for 2D drawing as OpenSCAD uses for 3D models
with python scripts instead of separate file format.

Usage
-----
```python
from gen_draw import *
from gen_draw.coords import Coords as C


# main drawing
d = Drawing()
dc = d.getCoords()

d.add(shapes.Line(
    dc,
    (5, 1),
    {'stroke': 'black'}
))
d.add(shapes.Rectangle(
    C(dc, (5, 3)),
    20,
    15
))
d.add(shapes.Rectangle(
    C(dc, (15, 13)),
    10,
    16,
    (5, 4)
))

# sub-drawing
d2 = Drawing(C(dc, (10, 10)))
d2c = d2.getCoords()
d.add(d2)

d2.add(shapes.Circle(
    d2c,
    5,
    {'fill': 'none'}
))
d2.add(shapes.Ellipse(
    C(d2c, (5,5)),
    (3, 7)
))
d2.add(shapes.Text(
    C(d2c, (10,10)),
    'Hello World!>',
    30,
    {
      'fill': 'red',
      'style': "font-size:12pt; font-weight:bold;"
    }
))


print(
  compilers.SVG(d)
)
```

License
-------
This program is licensed under the GNU General Public License version 3 or (at your option) any later version (GNU GPLv3+).

See file [LICENSE](LICENSE).

> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
> 
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
> 
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <http://www.gnu.org/licenses/>.
